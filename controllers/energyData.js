const { EnergyMessage } = require ('../models/EnergyModel');
const ErrorResponse = require("../utils/errorResponse");

//All the handle routs
 const getEnergyData = async (req, res) => {
    try {
        const energyMessage = await EnergyMessage.find();
        res.status(200).json(energyMessage)
    } catch (error) {
        res.status(404).json({message: error.message})
    }
}

const createEnergyData = async (req, res) => {
    const { title, message, value, indexEnergy } = req.body;

    try {
        const energy = await EnergyMessage.create({
            title, message, value, indexEnergy
        })

        res.status(201).json({
            succes: true,
            energy
        })
    } catch (error) {
        res.status(500).json({
            succes: false,
            error: error.message
        })
    }
}

const updateEnergyData = async (req, res, next) => {
    const {  title, message, value, indexEnergy } = req.body;

    if (!title || !message || !value || !indexEnergy)
    {
            return next(new ErrorResponse("Data does not correspond with the DataBase", 400))
    }
    
    try{
        const energyUpdate = await EnergyMessage.updateOne (
                //request for :_id
                req.params, 
                {
                    $set:req.body
                }
            )
        res.status(202).json(energyUpdate)
        }
    catch (error) {
            return  next(new ErrorResponse("Error while updating values", 400))
        }
};

const deleteEnergyData = async (req, res, next) => {
    const {  title, message, value, indexEnergy} = req.body;

    if (!title || !message || !value || !indexEnergy)
    {
            return next(new ErrorResponse("Data does not correspond with the DataBase", 401))
    }

    try{
        const energyRemove = await EnergyMessage.deleteOne(
                req.params
            )
        res.status(203).json(energyRemove)
        }
    catch (error) {
            return  next(new ErrorResponse("Error while deleting values", 401))
        }
};
        

module.exports = {
    getEnergyData,
    createEnergyData,
    updateEnergyData,
    deleteEnergyData
}