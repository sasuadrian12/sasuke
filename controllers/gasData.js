const { GasMessage } = require ('../models/GasModel');
const ErrorResponse = require("../utils/errorResponse");

//All the handle routs
 const getGasData = async (req, res) => {
    try {
        const gasMessage = await GasMessage.find();
        res.status(200).json(gasMessage)
    } catch (error) {
        res.status(404).json({message: error.message})
    }
}

const createGasData = async (req, res) => {
    const { title, message, value, indexGas } = req.body;

    try {
        const gas = await GasMessage.create({
            title, message, value, indexGas
        })

        res.status(201).json({
            succes: true,
            gas
        })
    } catch (error) {
        res.status(500).json({
            succes: false,
            error: error.message
        })
    }
}
const updateGasData = async (req, res, next) => {
    const {  title, message, value, indexGas } = req.body;

    if (!title || !message || !value || !indexGas)
    {
            return next(new ErrorResponse("Data does not correspond with the DataBase", 400))
    }
    try{
        const gasUpdate = await GasMessage.updateOne (
                //request for :_id
                req.params, 
                {
                    $set:req.body
                }
            )
        res.status(202).json(gasUpdate)
        }
    catch (error) {
            return  next(new ErrorResponse("Error while updating values", 400))
        }
};

const deleteGasData = async (req, res, next) => {
    const {  title, message, value, indexGas} = req.body;

    if (!title || !message || !value || !indexGas)
    {
            return next(new ErrorResponse("Data does not correspond with the DataBase", 401))
    }
    
    try{
        const gasRemove = await GasMessage.deleteOne(
                req.params
            )
        res.status(203).json(gasRemove)
        }
    catch (error) {
            return  next(new ErrorResponse("Error while deleting values", 401))
        }
    };

module.exports = {
    getGasData,
    createGasData,
    updateGasData,
    deleteGasData
}