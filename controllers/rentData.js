const { RentMessage } = require ('../models/RentModel');
const ErrorResponse = require("../utils/errorResponse");

//All the handle routs
 const getRentData = async (req, res) => {
    try {
        const rentMessage = await RentMessage.find();
        res.status(200).json(rentMessage)
    } catch (error) {
        res.status(404).json({message: error.message})
    }
}

const createRentData = async (req, res) => {
    const { title, message, value } = req.body;

    try {
        const rent = await RentMessage.create({
            title, message, value
        })

        res.status(201).json({
            succes: true,
            rent
        })
    } catch (error) {
        res.status(500).json({
            succes: false,
            error: error.message
        })
    }
}
const updateRentData = async (req, res, next) => {
    const {  title, message, value } = req.body;

    if (!title || !message || !value )
    {
            return next(new ErrorResponse("Data does not correspond with the DataBase", 400))
    }

    try{
         const rentUpdate = await RentMessage.updateOne (
                //request for :_id
                req.params, 
                {
                    $set:req.body
                }
            )
            res.status(202).json(rentUpdate)
        }
    catch (error) {
            return  next(new ErrorResponse("Error while updating values", 400))
        }
};

const deleteRentData = async (req, res, next) => {
    const {  title, message, value} = req.body;

    if (!title || !message || !value)
    {
            return next(new ErrorResponse("Data does not correspond with the DataBase", 401))
    }

    try{
        const rentRemove = await RentMessage.deleteOne(
                req.params
            )
        res.status(203).json(rentRemove)
        }
    catch (error) {
            return  next(new ErrorResponse("Error while deleting values", 401))
        }
};

module.exports = {
    getRentData,
    createRentData,
    updateRentData,
    deleteRentData
}