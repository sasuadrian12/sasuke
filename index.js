
const express = require("express")
const mongoose = require("mongoose")
const cors = require ("cors")

const errorHandler = require("./middleware/error");

const app = express();

require('dotenv').config();

app.use(express.json({ limit: '30mb', extended: true }));
app.use(express.urlencoded({ limit: '30mb', extended: true }));
app.use(cors());

app.use('/api/utilities', require('./routes/utilities'))
app.use('/api/auth', require('./routes/auth'));
app.use('/api/private', require('./routes/private'));

// heroku deploy message

app.get('/', (req, res) => {
  res.send('Hello to Sasuke API')
});

// Error handle should be the last piece of middleware

app.use(errorHandler);

const CONNECTION_URL = process.env.ATLAS_URL;
const PORT = process.env.PORT || 5001;

mongoose.connect(CONNECTION_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => app.listen(PORT, () => console.log(`Server Running on Port: http://localhost:${PORT}`)))
  .catch((error) => console.log(`${error} did not connect`));