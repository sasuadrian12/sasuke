const ErrorResponse = require('../utils/errorResponse');

const errorHandler = (err, req, res, next) => {
    let error = { ...err };

    error.message = err.message

    console.log(error)

    // 11000 in mongoose is duplicate error message
    if (err.code === 11000) {
        const message = 'Duplicate field value enter'
        error = new ErrorResponse(message, 400);
    }

    // in mongoose, ValidationError is a nesty obj 
    if(err.name === "ValidationError") {
        const message = Object.values(err.errors).map((val) => val.message);
        error = new ErrorResponse(message, 400);
    }

    res.status(error.statusCode || 500).json({
        succes: false,
        error: error.message || "Server error"
    })
}

module.exports = errorHandler;