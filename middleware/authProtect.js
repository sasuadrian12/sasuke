const jwt = require ('jsonwebtoken');
const User = require ('../models/User');
const ErrorResponse = require ('../utils/errorResponse')

//Check for jsonwebtoken in headers

exports.protect = async (req, res, next) => {
    let token;

    if(req.headers.authorization && req.headers.authorization.startsWith("Bearer")) {
        //add a bearer in front of the token and take the second array
        token = req.headers.authorization.split(" ")[1]

         //output --> Bearer 1251616851681
    }

    if (!token) {
        return next(new ErrorResponse("Not authorized to acces this route"), 401);
    }

    try {
        //decode the code that you will catch
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        const user = await User.findById(decoded.id)

        if(!user) {
            return next(new ErrorResponse("No user found with this id"), 404);
        }

        req.user = user;
        next()

    } catch (error) {
        return next(new ErrorResponse("Not authorized to acces this route"), 401);
    }
};
