const express = require('express');
router = express.Router();

//imports
const { getEnergyData, createEnergyData, updateEnergyData, deleteEnergyData } = require("../controllers/energyData.js");
const { getGasData, createGasData, updateGasData, deleteGasData } = require("../controllers/gasData.js")
const { getRentData, createRentData, updateRentData, deleteRentData } = require("../controllers/rentData.js")


//energy
router.route("/energy").get(getEnergyData);
router.route("/energy").post(createEnergyData);
router.route("/energy/:_id").put(updateEnergyData);
router.route("/energy/:_id").delete(deleteEnergyData);

//gas
router.route("/gas").get(getGasData);
router.route("/gas").post(createGasData);
router.route("/gas/:_id").put(updateGasData);
router.route("/gas/:_id").delete(deleteGasData);

//rent
router.route("/rent").get(getRentData);
router.route("/rent").post(createRentData);
router.route("/rent/:_id").put(updateRentData);
router.route("/rent/:_id").delete(deleteRentData);




module.exports = router;