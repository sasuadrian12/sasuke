const mongoose = require('mongoose');

const gasSchema = new mongoose.Schema({
    title: String,
    message: String,
    value: Number,
    indexGas: Number,
    createAt: {
        type: Date,
        default: new Date()
    },
});

const GasMessage = mongoose.model('GasMessage', gasSchema);

module.exports = {
    GasMessage
}