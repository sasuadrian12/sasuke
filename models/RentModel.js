const mongoose = require('mongoose');

const rentSchema = new mongoose.Schema({
    title: String,
    message: String,
    value: Number,
    createAt: {
        type: Date,
        default: new Date()
    },
});

const RentMessage = mongoose.model('RentMessage', rentSchema);

module.exports = {
    RentMessage
}