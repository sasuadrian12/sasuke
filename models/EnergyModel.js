const mongoose = require('mongoose');

const energySchema = new mongoose.Schema({
    title: String,
    message: String,
    value: Number,
    indexEnergy: Number,
    createAt: {
        type: Date,
        default: new Date()
    },
});

const EnergyMessage = mongoose.model('EnergyMessage', energySchema);

module.exports = {
    EnergyMessage
}